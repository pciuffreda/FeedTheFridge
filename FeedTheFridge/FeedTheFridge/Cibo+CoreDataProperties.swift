//
//  Cibo+CoreDataProperties.swift
//  FeedTheFridge
//
//  Created by Pasquale Ciuffreda on 20/10/16.
//  Copyright © 2016 Pasquale Ciuffreda. All rights reserved.
//

import Foundation
import CoreData 

extension Cibo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cibo> {
        return NSFetchRequest<Cibo>(entityName: "Cibo");
    }

    @NSManaged public var idCibo: Int64
    @NSManaged public var idFoto: String?
    @NSManaged public var nome: String?
    @NSManaged public var quantita: Int16
    @NSManaged public var cassetto: Cassetto?
    @NSManaged public var categoria: Categoria?
    @NSManaged public var dataScadenza: NSDate?

}
