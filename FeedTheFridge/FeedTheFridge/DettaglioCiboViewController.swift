import UIKit
import CoreData

class DettaglioCiboViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  
  var dataSource: CassettoDataSource!
  var cassetto: Cassetto!
  var imageStore: ImageStore!
  
  @IBOutlet var nomeCibo: UITextField?
  
  var quantitaCiboSelezionata: Int16 = 1
  @IBOutlet var quantitaPicker: UIPickerView!
  var pickerData : [String] = [String]()
  
  @IBOutlet var dataScadenzaPicker: UIDatePicker?
  
  @IBOutlet var imageView: UIImageView?
  
  var isUpdateCibo = false
  
  var cibo: Cibo? {
    didSet {
      navigationItem.title = cibo?.nome
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.quantitaPicker.delegate = self
    self.quantitaPicker.dataSource = self
    self.nomeCibo?.delegate = self
    
    //inizializzo il datasource del picker quantità
    for i in 1...99 {
      pickerData.append(String(i))
    }
    
    if let cibo = self.cibo {
      nomeCibo?.text = cibo.nome
      if cibo.quantita != 0 {
        quantitaCiboSelezionata = Int16(cibo.quantita)
      }
      
      if let idFoto = cibo.idFoto {
        let imageToDisplay = imageStore.imageForKey(key: idFoto)
        imageView?.image = imageToDisplay
      }
      
      if let dataScadenza = cibo.dataScadenza{
        //preseleziono il valore nel picker
        dataScadenzaPicker?.setDate(dataScadenza as Date, animated: true)
      } else {
        dataScadenzaPicker?.setDate(Date(), animated: true)
      }
    }
    
    dataScadenzaPicker?.addTarget(self, action: #selector(DettaglioCiboViewController.changeDataScadenza), for: .valueChanged)
    dataScadenzaPicker?.datePickerMode = .date
    dataScadenzaPicker?.locale = Locale(identifier: "it")
    dataScadenzaPicker?.minimumDate = NSDate() as Date
  }
  
  override func viewWillAppear(_ animated: Bool) {
    if let cibo = self.cibo{
      if cibo.quantita != 0 {
        //preseleziono il valore nel picker: lo faccio qui perchè sono sicuro che il picker è stato riempito
        quantitaPicker.selectRow(Int((cibo.quantita)) - 1, inComponent: 0, animated: true)
      }
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    
    //annullo il first responder
    view.endEditing(true)
  }
  
  //MARK: - Inserimento e aggiornamento in Core Data
  @IBAction func annullaInserimento(_ sender: AnyObject) {
    dismiss(animated: true, completion: nil)
    
    if !isUpdateCibo {
      //cancellare l'immagine dallo store
      if let idFoto = self.cibo?.idFoto {
        imageStore.deleteImageForKey(key: (idFoto))
      }
      
      //cancellare l'oggetto cibo dal datasource
      if let ciboDaCancellare = self.cibo {
        let context = self.dataSource.coreDataStack.mainQueueContext
        context.delete(ciboDaCancellare)
        
        do{
          try self.dataSource.coreDataStack.saveChanges()
        } catch let error {
          print("Errore cancellazione cibo dal datasource con Core Data: \(error)")
        }
      }
    }
  }
  
  @IBAction func inserisciCibo(_ sender: AnyObject) {
    let context = self.dataSource.coreDataStack.mainQueueContext
    
    if isUpdateCibo {
      self.cibo?.nome = nomeCibo?.text
      self.cibo?.quantita = quantitaCiboSelezionata
      self.cibo?.dataScadenza = dataScadenzaPicker?.date as NSDate?
    } else {
        if (self.cibo == nil) {
            let newCibo = NSEntityDescription.insertNewObject(forEntityName: "Cibo", into: context)
            newCibo.setValue(cassetto, forKey: "cassetto")
            newCibo.setValue(NSNumber(value: arc4random()), forKey: "idCibo")
            newCibo.setValue(nomeCibo?.text, forKey: "nome")
            newCibo.setValue(NSNumber(value: quantitaCiboSelezionata), forKey: "quantita")
            newCibo.setValue(self.cibo?.idFoto!, forKey: "idFoto")
            newCibo.setValue(dataScadenzaPicker?.date, forKey: "dataScadenza")
            /*
             DA COMPLETARE
             */
            //newCibo.setValue(nomeCibo.text, forKey: "categoria")
            //
        }
    }
    
    do{
      try self.dataSource.coreDataStack.saveChanges()
    } catch let error {
      print("Errore in salvataggio con Core Data: \(error)")
    }
    
    dismiss(animated: true, completion: nil)
  }
  
  //MARK: - Gestione immagine
  @IBAction func scattaFoto(_ sender: AnyObject) {
    let imagePicker = UIImagePickerController()
    
    if UIImagePickerController.isSourceTypeAvailable(.camera){
      imagePicker.sourceType = .camera
    } else {
      imagePicker.sourceType = .photoLibrary
    }
    imagePicker.delegate = self
    
    present(imagePicker, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    let image = info[UIImagePickerControllerOriginalImage] as! UIImage
    
    imageView?.image = image
    
    if !isUpdateCibo {
      if (self.cibo == nil) {
        let context = self.dataSource.coreDataStack.mainQueueContext
        let entityDescription = NSEntityDescription.entity(forEntityName: "Cibo", in: context)
        self.cibo = NSManagedObject(entity: entityDescription!, insertInto: context) as? Cibo
      }
      self.cibo?.cassetto = self.cassetto
      if let nomeCiboText = nomeCibo?.text, !(nomeCibo?.text?.isEmpty)! {
        self.cibo?.nome = nomeCiboText
      }
      self.cibo?.quantita = quantitaCiboSelezionata
    }
    
    self.cibo?.idFoto = NSUUID().uuidString
    self.cibo?.dataScadenza = dataScadenzaPicker?.date as NSDate?
    
    imageStore.setImage(image: image, forKey: (self.cibo?.idFoto!)!)
    
    dismiss(animated: true, completion: nil)
  }
  
  //MARK: - Tap sullo sfondo nasconde la tastiera
  @IBAction func backgroundTapped(_ sender: UITapGestureRecognizer) {
    view.endEditing(true)
  }
  
  //MARK: - Picker quantità
  //numero di colonne del picker
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  //numero di elementi nel picker
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pickerData.count
  }
  
  //elemento da visualizzare dal datasource
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return pickerData[row]
  }
  
  //elemento selezionato al cambio di valore
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      quantitaCiboSelezionata = Int16(pickerData[row])!
  }
  
  func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 20
  }
  
  //MARK: - Picker data scadenza
  func changeDataScadenza(){
    self.cibo?.dataScadenza = dataScadenzaPicker?.date as NSDate?
  }
}
