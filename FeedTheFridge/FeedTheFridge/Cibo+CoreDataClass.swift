//
//  Cibo+CoreDataClass.swift
//  FeedTheFridge
//
//  Created by Pasquale Ciuffreda on 20/10/16.
//  Copyright © 2016 Pasquale Ciuffreda. All rights reserved.
//

import Foundation
import CoreData


public class Cibo: NSManagedObject {
  
  public override func awakeFromInsert() {
    idCibo = 0
    idFoto = ""
    nome = ""
    quantita = 0
    cassetto = nil
    categoria = nil
    dataScadenza = nil
  }
}
