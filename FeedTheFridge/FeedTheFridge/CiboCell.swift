import UIKit

class CiboCell: UITableViewCell {
  
  @IBOutlet var nomeCiboLabel: UILabel!
  @IBOutlet var categoriaCiboLabel: UILabel!
  @IBOutlet var quantitaCiboLabel: UILabel!
  @IBOutlet var ciboImage: UIImageView!
  
  
  //aggiorno il font in base a quello scelto dall'utente nelle preferenze di sistema
  func updateLabels(){
    let bodyFont = UIFont.preferredFont(forTextStyle: .body)
    nomeCiboLabel.font = bodyFont
    quantitaCiboLabel.font = bodyFont
    
    let caption1Font = UIFont.preferredFont(forTextStyle: .caption1)
    categoriaCiboLabel.font = caption1Font
  }
}
