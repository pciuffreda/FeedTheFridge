import Foundation
import CoreData

class CoreDataStack {
  
  let managedObjectModelName: String
  
  private lazy var managedObjectModel: NSManagedObjectModel = {
    let modelURL = Bundle.main.url(forResource: self.managedObjectModelName, withExtension: "momd")
    return NSManagedObjectModel(contentsOf: modelURL!)!
  }()
  
  private var applicationDocumentsDirectory: NSURL = {
    let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return urls.first! as NSURL
  }()
  
  private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
    var coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
    let pathComponent = "\(self.managedObjectModelName).sqlite"
    let url = self.applicationDocumentsDirectory.appendingPathComponent(pathComponent)
    let store = try! coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
    
    return coordinator
  }()
  
  //context principale
  /*
   context associato ad un persistent store cooridnator
   ogni modifica fatta in questo contesto viene persistita automaticamente
   */
  lazy var mainQueueContext: NSManagedObjectContext = {
    let moc = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
    moc.persistentStoreCoordinator = self.persistentStoreCoordinator
    moc.name = "Main Queue Context (UI Context)"
    
    return moc
  }()
  
  //context delle operazioni in background
  /*
   questo context è associato a un altro context in un rapporto padre/figlio
   ogni modifica effettuata in questo contesto si propaga automaticamente al padre
   ma queste modifiche non vengono salvate automaticamente ma bisogna
   farlo manualmente
   */
  lazy var privateQueueContext: NSManagedObjectContext = {
    let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
    moc.parent = self.mainQueueContext
    moc.name = "Primary Private Queue Context"
    
    return moc
  }()
  
  //MARK: - Core Data Stack Init
  required init(modelName: String){
    managedObjectModelName = modelName
  }
  
  //MARK: - Salvare i cambiamenti
  func saveChanges() throws {
    var error: Error?
    
    privateQueueContext.performAndWait() { () -> Void in
      if self.privateQueueContext.hasChanges {
        do {
          try self.privateQueueContext.save()
        } catch let saveError {
          error = saveError
        }
      }
    }
    
    if let error = error {
      throw error
    }
    
    mainQueueContext.performAndWait() {
      if self.mainQueueContext.hasChanges {
        do {
          try self.mainQueueContext.save()
        } catch let saveError {
          error = saveError
        }
      }
    }
    if let error = error {
      throw error
    }
  }
}
