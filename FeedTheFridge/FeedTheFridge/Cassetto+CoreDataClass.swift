import Foundation
import CoreData


public class Cassetto: NSManagedObject {
  
  public override func awakeFromInsert() {
    super.awakeFromInsert()
    
    idCassetto = 0
    nome = ""
    posizione = 0
    totale = 0
    cibo = nil
  }

}
