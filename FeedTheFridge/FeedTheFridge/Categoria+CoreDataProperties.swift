//
//  Categoria+CoreDataProperties.swift
//  FeedTheFridge
//
//  Created by Pasquale Ciuffreda on 20/10/16.
//  Copyright © 2016 Pasquale Ciuffreda. All rights reserved.
//

import Foundation
import CoreData 

extension Categoria {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Categoria> {
        return NSFetchRequest<Categoria>(entityName: "Categoria");
    }

    @NSManaged public var descrizione: String?
    @NSManaged public var idCategoria: Int16
    @NSManaged public var cibo: NSSet?

}

// MARK: Generated accessors for cibo
extension Categoria {

    @objc(addCiboObject:)
    @NSManaged public func addToCibo(_ value: Cibo)

    @objc(removeCiboObject:)
    @NSManaged public func removeFromCibo(_ value: Cibo)

    @objc(addCibo:)
    @NSManaged public func addToCibo(_ values: NSSet)

    @objc(removeCibo:)
    @NSManaged public func removeFromCibo(_ values: NSSet)

}
