import UIKit

class AboutController: UIViewController {
  
  @IBOutlet var menuButton: UIBarButtonItem!
  
  override func viewDidLoad() {
    //attivazione menu slide laterale
    if revealViewController() != nil {
      menuButton.target = revealViewController()
      menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
      
      revealViewController().rightViewRevealWidth = 300
      
      view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
  }
}
