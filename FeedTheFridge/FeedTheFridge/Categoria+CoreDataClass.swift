//
//  Categoria+CoreDataClass.swift
//  FeedTheFridge
//
//  Created by Pasquale Ciuffreda on 20/10/16.
//  Copyright © 2016 Pasquale Ciuffreda. All rights reserved.
//

import Foundation
import CoreData


public class Categoria: NSManagedObject {
  
  public override func awakeFromInsert() {
    super.awakeFromInsert()
    
    descrizione = ""
    idCategoria = 0
    cibo = nil
  }

}
