import UIKit

class ImageStore {
  
  //cache di immagini da salvare su disco e che verranno richiamate solo quando necessario
  let cache = NSCache<NSString,AnyObject>()
  
  func setImage(image: UIImage, forKey key: String){
    cache.setObject(image, forKey: key as NSString)
    
    //creo l'URL completo dell'immagine
    let imageURL = imageUrlForKey(key: key)
    
    //converto l'immagine in JPEG
    if let data = UIImageJPEGRepresentation(image, 0.5) {
      do {
        //salvo il data all'URL
        try data.write(to: imageURL as URL, options: .atomic)
      } catch let error {
        print("Errore nel salvataggio dell'immagine: \(error)")
      }
    }
  }
  
  func imageForKey(key: String) -> UIImage? {
    if let existingImage = cache.object(forKey: key as NSString) as? UIImage {
      return existingImage
    }
    
    let imageUrl = imageUrlForKey(key: key)
    guard let imageFromDisk = UIImage(contentsOfFile: imageUrl.path!) else {
      return nil
    }
    cache.setObject(imageFromDisk, forKey: key as NSString)
    return imageFromDisk
  }
  
  func deleteImageForKey(key: String) {
    cache.removeObject(forKey: key as NSString)
    
    let imageUrl = imageUrlForKey(key: key)
    do{
      try FileManager.default.removeItem(at: imageUrl as URL)
    } catch let deleteError {
      print("Errore nella cancellazione dell'immagine dal disco: \(deleteError)")
    }
    
  }
  
  //MARK: - Archiviazione
  func imageUrlForKey(key: String) -> NSURL {
    let documentDirectories = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentdirectory = documentDirectories.first!
    return documentdirectory.appendingPathComponent(key) as NSURL
  }
}
