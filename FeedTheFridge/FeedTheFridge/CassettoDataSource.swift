import UIKit
import CoreData

class CassettoDataSource {
  
  var cassetti = [Cassetto]()
  
  let coreDataStack = CoreDataStack(modelName: "FeedTheFridge")
  
  func fetchCassetti(predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = nil) throws -> [Cassetto]{
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Cassetto")
    fetchRequest.sortDescriptors = sortDescriptors
    fetchRequest.predicate = predicate
    
    let mainQueueContext = self.coreDataStack.mainQueueContext
    var mainQueueCassetti: [Cassetto]?
    var fetchRequestError: Error?
    mainQueueContext.performAndWait {
      do {
        mainQueueCassetti = try mainQueueContext.fetch(fetchRequest) as? [Cassetto]
      } catch let error {
        fetchRequestError = error
      }
    }
    
    guard let cassetti = mainQueueCassetti else {
      throw fetchRequestError!
    }
    return cassetti
  }
}
