//
//  Cassetto+CoreDataProperties.swift
//  FeedTheFridge
//
//  Created by Pasquale Ciuffreda on 20/10/16.
//  Copyright © 2016 Pasquale Ciuffreda. All rights reserved.
//

import Foundation
import CoreData 

extension Cassetto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cassetto> {
        return NSFetchRequest<Cassetto>(entityName: "Cassetto");
    }

    @NSManaged public var idCassetto: Int64
    @NSManaged public var nome: String?
    @NSManaged public var posizione: Int16
    @NSManaged public var totale: Int16
    @NSManaged public var cibo: NSSet?

}

// MARK: Generated accessors for cibo
extension Cassetto {

    @objc(addCiboObject:)
    @NSManaged public func addToCibo(_ value: Cibo)

    @objc(removeCiboObject:)
    @NSManaged public func removeFromCibo(_ value: Cibo)

    @objc(addCibo:)
    @NSManaged public func addToCibo(_ values: NSSet)

    @objc(removeCibo:)
    @NSManaged public func removeFromCibo(_ values: NSSet)

}
