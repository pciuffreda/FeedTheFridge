import UIKit
import CoreData

class CassettoViewController: UITableViewController {
  
  var dataSource: CassettoDataSource = CassettoDataSource()
  
  var imageStore: ImageStore = ImageStore()
  
  @IBOutlet var menuButton: UIBarButtonItem!
  
  @IBOutlet var editButton: UIBarButtonItem!
  
  //MARK: - Initializers
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    editButton = editButtonItem
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let ordinamentoPosizionale = NSSortDescriptor(key: "idCassetto", ascending: true)
    let cassetti = try! self.dataSource.fetchCassetti(predicate: nil, sortDescriptors: [ordinamentoPosizionale])
    
    self.dataSource.cassetti = cassetti
    
    self.tableView.reloadData()
    
    //attivazione menu slide laterale
    if revealViewController() != nil {
      menuButton.target = revealViewController()
      menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
      
      revealViewController().rightViewRevealWidth = 300
      
      view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.tableView.reloadData()
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.dataSource.cassetti.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
    
    let cassetto = dataSource.cassetti[indexPath.row]
    
    var totale = 0
    for cibo in cassetto.cibo! {
      totale += Int((cibo as! Cibo).quantita)
    }
    
    cell.textLabel?.text = cassetto.nome
    cell.detailTextLabel?.text = String(totale)
    
    return cell
  }
  
  @IBAction func aggiungiCassetto(_ sender: AnyObject) {
    let alertController = UIAlertController(title: "Nome Cassetto", message: nil, preferredStyle: .alert)
    
    alertController.addTextField(configurationHandler: {(textField) -> Void in
      textField.placeholder = "nome cassetto"
      textField.autocapitalizationType = .words
    })
    
    let okAction = UIAlertAction(title: "OK", style: .default, handler: {(action) -> Void in
      
      if let tagName = alertController.textFields?.first?.text {
        let context = self.dataSource.coreDataStack.mainQueueContext
        
        let newTag = NSEntityDescription.insertNewObject(forEntityName: "Cassetto", into: context)
        newTag.setValue(tagName, forKey: "nome")
        newTag.setValue(1, forKey: "posizione")
        newTag.setValue(NSNumber(value: arc4random()), forKey: "idCassetto")
        newTag.setValue(1, forKey: "totale")
        
        do{
          try self.dataSource.coreDataStack.saveChanges()
        } catch let error {
          print("Errore in salvataggio con Core Data: \(error)")
        }
        
        self.updateCassetti()
        
        self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
      }
    })
    
    alertController.addAction(okAction)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    
    present(alertController, animated: true, completion: nil)
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let cassettoDaEliminare = self.dataSource.cassetti[indexPath.row]
      
      let context = self.dataSource.coreDataStack.mainQueueContext
      context.delete(cassettoDaEliminare)
      
      self.dataSource.cassetti.remove(at: indexPath.row)
      
      do{
        try self.dataSource.coreDataStack.saveChanges()
      } catch let error {
        print("Errore in salvataggio con Core Data: \(error)")
      }
      
      self.updateCassetti()
      
      self.tableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
    }
  }
  
  func updateCassetti(){
    let cassetti = try! dataSource.fetchCassetti(predicate: nil, sortDescriptors: [NSSortDescriptor(key: "idCassetto", ascending: true)])
    dataSource.cassetti = cassetti
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "MostraElencoCibo" {
      if let row = tableView.indexPathForSelectedRow?.row {
        let cassetto = dataSource.cassetti[row]
        let navigationController = segue.destination as! UINavigationController
        let elencoCibiViewController = navigationController.topViewController as! ElencoCibiViewController
        elencoCibiViewController.cassetto = cassetto
        elencoCibiViewController.dataSource = self.dataSource
        elencoCibiViewController.imageStore = imageStore
      }
    }
  }
}
