import UIKit
import CoreData

class ElencoCibiViewController: UITableViewController {
  
  var cassetto: Cassetto! {
    didSet {
      navigationItem.title = cassetto.nome
    }
  }
  
  var dataSource: CassettoDataSource!
  
  var imageStore: ImageStore!
  
  var cibiDataSource: [Cibo] {
    var cibiToReturn = [Cibo]()
    if let cassettoDataSource = cassetto {
      if cassettoDataSource.cibo != nil && (cassettoDataSource.cibo?.count)! > 0{
        cibiToReturn = cassettoDataSource.cibo?.allObjects as! [Cibo]
      }
    }
    return cibiToReturn
  }
  
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cibiDataSource.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "CiboCell", for: indexPath) as! CiboCell
    
    //aggiorno i font dei labels della riga
    cell.updateLabels()
    
    let cibo = cibiDataSource[indexPath.row]
    
    cell.nomeCiboLabel.text = cibo.nome
    cell.quantitaCiboLabel?.text = String(cibo.quantita)
    if let categoria = cibo.categoria {
        cell.categoriaCiboLabel.text = categoria.descrizione
    } else {
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      formatter.locale = Locale(identifier: "it")
      cell.categoriaCiboLabel.text = formatter.string(for: cibo.dataScadenza)
    }
    
    let scaledImage: UIImage
    
    if let idFoto = cibo.idFoto {
      let image = imageStore.imageForKey(key: idFoto)
      
      let size = (image?.size)!.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
      let hasAlpha = false
      let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
      
      UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
      image?.draw(in: CGRect(origin: CGPoint.zero, size: size))
      
      scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
      UIGraphicsEndImageContext()
    } else {
      let image = UIImage(named: "no-thumb.png")
      
      let size = (image?.size)!.applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
      let hasAlpha = false
      let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
      
      UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
      image?.draw(in: CGRect(origin: CGPoint.zero, size: size))
      
      scaledImage = UIGraphicsGetImageFromCurrentImageContext()!
      UIGraphicsEndImageContext()
    }
    
    cell.ciboImage.image = scaledImage
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    return true
  }
  
  override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    if editingStyle == .delete {
      let ciboDaEliminare = cibiDataSource[indexPath.row]
      let context = self.dataSource.coreDataStack.mainQueueContext
      context.delete(ciboDaEliminare)
      
      do{
        try self.dataSource.coreDataStack.saveChanges()
      } catch let error {
        print("Errore in salvataggio con Core Data: \(error)")
      }
      
      tableView.reloadData()
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(ElencoCibiViewController.goBack))
            
    tableView.reloadData()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = 65
  }
  
  func goBack(){
    dismiss(animated: true, completion: nil)
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "AggiungiNuovoCibo" {
      let navigationController = segue.destination as! UINavigationController
      let dettaglioCiboViewController = navigationController.topViewController as! DettaglioCiboViewController
      dettaglioCiboViewController.dataSource = self.dataSource
      dettaglioCiboViewController.cassetto = self.cassetto
      dettaglioCiboViewController.imageStore = imageStore
      dettaglioCiboViewController.isUpdateCibo = false
    } else if segue.identifier == "MostraCiboSelezionato" {
      if let row = tableView.indexPathForSelectedRow?.row {
        let cibo = cibiDataSource[row]
        let navigationController = segue.destination as! UINavigationController
        let dettaglioCiboViewController = navigationController.topViewController as! DettaglioCiboViewController
        dettaglioCiboViewController.dataSource = self.dataSource
        dettaglioCiboViewController.cassetto = self.cassetto
        dettaglioCiboViewController.cibo = cibo
        dettaglioCiboViewController.imageStore = imageStore
        dettaglioCiboViewController.isUpdateCibo = true
      }
    }
  }
}
